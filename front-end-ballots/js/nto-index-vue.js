// indexMV_* --> model-view variables. commonly user input with single/double reactive binding
// indexVar_* --> non-model, unbound, local or global variables

var config = JSON.parse(conf);
var indexVar_server = config["protocol"] + "://" + config["address"] + ":" + config["port"];
var indexVar_token = null;
var indexVar_localstorage = {
	"user_data": null,
	"votes": null 
}

var mainView = new Vue({
	el: "#indexVue_main",
	data: {
		indexMV_id_number: ""
	},
	methods: {
		indexClick_login: function(){
			$.ajax({
				method: "get",
				url: indexVar_server + "/nto/get/voter-login",
				data: {id_number: this.indexMV_id_number}
			})
			.done(function(data, textStatus, xhr){
				if(data["data"] != null){ 
					indexVar_token = data["data"][0]["gen_id"];

					// create localstorage variable
					indexVar_localstorage["user_data"] = data["data"];

					localStorage.setItem("lstorage", JSON.stringify(indexVar_localstorage));
					localStorage.setItem("token", indexVar_token);
					location.replace("/election-proper")
				}
				else{ 
					indexVar_token = null; 
					UIkit.notification({ message: "<small>"+data["message"]+"</small>", status: "danger", pos: "bottom-left", timeout: 3000 });
				}
			});
		}
	}
});