// vstatsMV_* --> model-view variables. commonly user input with single/double reactive binding
// vstatsVar_* --> non-model, unbound, local or global variables

var config = JSON.parse(conf);
var vstatsVar_server = config["protocol"] + "://" + config["address"] + ":" + config["port"];

// bTcHToy6gTESnkRxnxEZB7J1RxEq0VT8P9oyt7kYwuvwaxvxfwsH3bJglGRwg4mxALOKsh8bZIZZF6Hv3kCzhGwPFZSvypkegGMRr1IVmAAQ0JDqKDFCjiytkUaUQYQVsxfiryoXcdfwncajlAaQ3IapzpnzerXJF3Xikfm5qLSrZyhZ5wt5r26uCYQmkE3EcXEfaRmr
// 2015-0529
// 2010-0338

var mainView = new Vue({
	el: "#vstatsVue_main",
	data: {
		vstatsMV_sequence: null,
		vstatsMV_validFlag: false,
		vstatsMV_currentVoters: [],
		vstatsMV_currentSearch: null,
		vstatsMV_currentFeat: 1
	},
	methods: {
		vstatsFunc_validateSequence: function(){
			$.ajax({
				method: "get",
				url: vstatsVar_server + "/nto/get/validated/sequence",
				data: {seq: this.vstatsMV_sequence}
			})
			.done(function(data, textStatus, xhr){
				// when updating asynchronously, don't use 'this' instead use vm name directly.
				mainView.vstatsMV_validFlag = data["data"];
				if(mainView.vstatsMV_validFlag == false){ 
					UIkit.notification({ message: "<small>"+data["message"]+"</small>", status: "danger", pos: "bottom-left", timeout: 3000 });
				}
			});
		},
		vstatsFunc_getVoter: function(){
			$.ajax({
				method: "post",
				url: vstatsVar_server + "/nto/get/voter",
				headers: { "Authorization": this.vstatsMV_sequence },
				data: { voter: this.vstatsMV_currentSearch }
			})
			.done(function(data, textStatus, xhr){
				// when updating asynchronously, don't use 'this' instead use vm name directly.
				mainView.vstatsMV_currentVoters = data["data"];
				console.log(mainView.vstatsMV_currentVoters);

				UIkit.notification({ message: "<small>"+data["message"]+"</small>", status: "danger", pos: "bottom-left", timeout: 3000 });
			});
		},
		vstatsFunc_voterAccess: function(voter){
			$.ajax({
				method: "post",
				url: vstatsVar_server + "/nto/update/voter",
				headers: { "Authorization": this.vstatsMV_sequence },
				data: voter
			})
			.done(function(data, textStatus, xhr){
				// when updating asynchronously, don't use 'this' instead use vm name directly.
				UIkit.notification({ message: "<small>"+data["message"]+"</small>", status: "danger", pos: "bottom-left", timeout: 3000 });
			});
		},
		vstatsFunc_getLogs: function(){
			$.ajax({
				method: "post",
				url: vstatsVar_server + "/nto/get/logs",
				headers: { "Authorization": this.vstatsMV_sequence }
			})
			.done(function(data, textStatus, xhr){
				// when updating asynchronously, don't use 'this' instead use vm name directly.
				var textarea = document.getElementById("logs_area");

				textarea.value = data["data"];
				UIkit.notification({ message: "<small>"+data["message"]+"</small>", status: "danger", pos: "bottom-left", timeout: 3000 });

				textarea.scrollTop = textarea.scrollHeight;
			});
		}
	},
	mounted: function(){

	}
});