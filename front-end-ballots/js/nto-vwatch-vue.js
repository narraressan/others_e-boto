// vwatchMV_* --> model-view variables. commonly user input with single/double reactive binding
// vwatchVar_* --> non-model, unbound, local or global variables

var config = JSON.parse(conf);
var vwatchVar_server = config["protocol"] + "://" + config["address"] + ":" + config["port"];

var mainView = new Vue({
	el: "#vwatchVue_main",
	data: {
		vwatchMV_results: [],
		vwatchMV_interval_results: null,
		vwatchMV_interval_ads: null,
	},
	methods: {
		vwatchFunc_voteCount: function(){
			$.ajax({
				method: "get",
				url: vwatchVar_server + "/nto/monitor/results"
			})
			.done(function(data, textStatus, xhr){
				// when updating asynchronously, don't use 'this' instead use vm name directly.
				if(data["data"] == null){ 
					UIkit.notification({ message: "<small>"+data["message"]+"</small>", status: "danger", pos: "bottom-left", timeout: 3000 });
				}
				else{ 
					mainView.vwatchMV_results = data["data"]; 
					console.log(mainView.vwatchMV_results);
				}
			});
		},
		vwatchFunc_showAds: function(){
			$("#add-show").click();
			console.log("show ads");
		},
	},
	mounted: function(){
		this.vwatchFunc_voteCount();
		this.vwatchMV_interval_results = setInterval(function(){ this.vwatchFunc_voteCount(); }.bind(this), 3000);

		this.vwatchMV_interval_ads = setInterval(function(){ this.vwatchFunc_showAds(); }.bind(this), 10000);
	}
});