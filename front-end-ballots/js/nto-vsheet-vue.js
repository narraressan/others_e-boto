// vsheetMV_* --> model-view variables. commonly user input with single/double reactive binding
// vsheetVar_* --> non-model, unbound, local or global variables

var config = JSON.parse(conf);
var vsheetVar_server = config["protocol"] + "://" + config["address"] + ":" + config["port"];

var mainView = new Vue({
	el: "#vsheetVue_main",
	data: {
		vsheetMV_random_bg: null,
		vsheetMV_currentPage: -1,
		vsheetMV_currentLimit: 0,
		vsheetMV_candidates: [],
		vsheetMV_filtered: [],
		vsheetMV_positions: {},
		vsheetMV_positionOrder: [],
		vsheetMV_votes: []
	},
	methods: {
		vsheetFunc_randomBG: function(min,max){
			return Math.floor(Math.random() * (max-min+1)+min);
		},
		vsheetClick_submit_confirm: function(){
			$(this).blur();
			UIkit.modal.confirm("Are you sure of your <i>votes</i>?").then(
				function(){ 
					var userData = JSON.parse(localStorage.getItem("lstorage"))["user_data"][0];
					userData["cast"] = mainView.vsheetMV_votes;

					// cast vote
					$.ajax({
						method: "post",
						url: vsheetVar_server + "/nto/add/votes",
						headers: { "Authorization": localStorage.getItem("token")},
						data: userData
					})
					.done(function(data, textStatus, xhr){
						// when updating asynchronously, don't use 'this' instead use vm name directly.
						if(data["data"] == 200){
							UIkit.modal.alert(data["message"]).then(function() {
								localStorage.clear();
								location.replace("/");
							});
						}
						else{
							$(this).blur();
							UIkit.notification({ message: "<small>"+data["message"]+"</small>", status: "danger", pos: "bottom-left", timeout: 3000 });
						}
					});
				},
				function(){ console.log('Rejected.');}
			);
		},
		vsheetClick_logout_confirm: function(){
			$(this).blur();
			UIkit.modal.confirm("Are you sure you are leaving?").then(
				function(){ 
					localStorage.clear();
					location.replace("/");
				},
				function(){ console.log('Rejected.');}
			);
		},
		vsheetFunc_getCandidates: function(){
			var userData = JSON.parse(localStorage.getItem("lstorage"))["user_data"][0];
			$.ajax({
				method: "post",
				url: vsheetVar_server + "/nto/get/candidates-list",
				headers: { "Authorization": localStorage.getItem("token")},
				data: userData
			})
			.done(function(data, textStatus, xhr){
				// when updating asynchronously, don't use 'this' instead use vm name directly.
				mainView.vsheetMV_candidates = data["data"];
			});
		},
		vsheetFunc_getPositions: function(){
			var userData = JSON.parse(localStorage.getItem("lstorage"))["user_data"][0];
			$.ajax({
				method: "post",
				url: vsheetVar_server + "/nto/get/positions-in-order",
				headers: { "Authorization": localStorage.getItem("token")},
				data: userData
			})
			.done(function(data, textStatus, xhr){
				// when updating asynchronously, don't use 'this' instead use vm name directly.
				mainView.vsheetMV_positions = JSON.parse(data["data"][0]["positions"]);
				mainView.vsheetMV_positionOrder = [];
				for(var k in mainView.vsheetMV_positions) mainView.vsheetMV_positionOrder.push(String(k).toUpperCase());
			});
		},
		vsheetFunc_filterPosition: function(page){
			var filtered = [];
			var position = 0;

			if(this.vsheetMV_currentPage == this.vsheetMV_positionOrder.length-1 && page > 0){ this.vsheetMV_currentPage = -1;}
			if(this.vsheetMV_currentPage <= 0 && page < 0){ this.vsheetMV_currentPage = this.vsheetMV_positionOrder.length;}

			position = this.vsheetMV_positionOrder[this.vsheetMV_currentPage += page];

			for (var j = 0; j < this.vsheetMV_candidates.length; j++) {
				if(String(this.vsheetMV_candidates[j]["position"]).toUpperCase() == String(position).toUpperCase()){ 
					filtered.push(this.vsheetMV_candidates[j]);
				}
			}
			this.vsheetMV_filtered = filtered;

			var limit = 0;
			var temp = Object.keys(this.vsheetMV_positions);
			for (var i = 0; i < temp.length; i++) {
				if(temp[i].toUpperCase() == position.toUpperCase()){ limit = this.vsheetMV_positions[temp[i]]; break;}
			}
			this.vsheetMV_currentLimit = limit;
		},
		vsheetFunc_countPositions: function(current_position){
			// count candidates with position from vsheetMV_votes
			var count = 0;
			for (var i = 0; i < this.vsheetMV_votes.length; i++) {
				var tempCand = this.vsheetMV_votes[i];
				var tempPos = null;

				for (var j = 0; j < this.vsheetMV_candidates.length; j++) {
					if(this.vsheetMV_candidates[j]["id_number"] == tempCand){ tempPos = this.vsheetMV_candidates[j]["position"];}
				}

				if(tempPos.toUpperCase() == current_position.toUpperCase()){ count++;}
			}
			return count;
		}
	},
	mounted: function(){
		this.vsheetMV_random_bg = "front-end-utilities/NTO-img/vsheet-" + this.vsheetFunc_randomBG(1, 4) + ".jpg";
		this.vsheetFunc_getCandidates();
		this.vsheetFunc_getPositions();
	}
});