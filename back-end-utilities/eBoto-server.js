// startup syntax
// node nto-boilerplate-server.js 
// nodemon nto-boilerplate-server.js

const reqExpress = require("express"); 
const reqIPFilter = require("express-ipfilter").IpFilter;
const reqbodyParser = require("body-parser");
const reqPath = require("path");
const reqMySQL = require("mysql");

// production-wise practice
var fs = require("fs");
var util = require("util");
var logName = "log.txt";
var logFile = fs.createWriteStream(logName, { flags: "a" });
var logStdout = process.stdout;
// customize console.log to record to text file for future references.
console.log = function () {
	logFile.write(util.format.apply(null, arguments) + "\n");
	logStdout.write(util.format.apply(null, arguments) + "\n");
}
console.error = console.log;


// make sure you have this user in your db users
var connection = reqMySQL.createConnection({ host: "192.168.99.1", port: "3306", user: "nto2017", password: "LionCity", database: "eboto_2017"});
// NTO default variables, configuration, utilities and resources
var express = reqExpress();
var electionID = "20-1718/msugscba&a+bsit@narraNTO";
var config = {
	"default-port": 600,
	"server-id": "#eBoto@2017-0129",
	"valid-ip": ["::ffff:192.168.99.1", "192.168.99.1"]
};
express.use(reqIPFilter(config["valid-ip"], {mode: "allow"}));
express.use(reqbodyParser.urlencoded({ extended: false }));

// vuejs source files
express.use("/front-end-ballots", reqExpress.static(reqPath.join(__dirname, "/../front-end-ballots")));

express.get("/", (req, res) => { res.sendFile(reqPath.join(__dirname + "/../front-end-ballots/index.html"));});
express.get("/admin", (req, res) => { res.sendFile(reqPath.join(__dirname + "/../front-end-ballots/vstats.html"));});
express.get("/election-proper", (req, res) => { res.sendFile(reqPath.join(__dirname + "/../front-end-ballots/vsheet.html"));});
express.get("/watch", (req, res) => { res.sendFile(reqPath.join(__dirname + "/../front-end-ballots/vwatch.html"));});
// ---------------------


// validate token
function validateToken(token, res, senderData, responseData, successCallback, errorCallback, voteCast){
	connection.query("SELECT * FROM `voters` WHERE `gen_id`=? and election_id=?", [token, electionID], function (error, results, fields) {
		if(error){ responseData["message"] = "Unable to establish secure connection with db."; }
		
		// check token if it exist and has no clones
		// check if voting access has been enabled
		// check if result content is equal to data.
		// this is a DIRTY CHECKING and is NOT the best way to check. Never use in production.
		if(results.length == 1 && 
			results[0]["voting_enabled"] == true && 
			results[0]["fullname"] == senderData["fullname"] && 
			results[0]["id_number"] == senderData["id_number"] && 
			results[0]["vote_cast"] == senderData["vote_cast"] && 
			results[0]["vote_cast"] == false && 
			results[0]["on_vote"] == null){
			successCallback();
		}
		else{ errorCallback();}
	});
}
function validateAdminToken(token, res, voter, responseData, successCallback, errorCallback){
	connection.query("SELECT * FROM `elections` WHERE `election_id`=? and `login_sequence`=?", [electionID, token], function (error, results, fields) {
		if(error){ responseData["message"] = "Unable to establish secure connection with db."; }
		
		if(results.length == 1){ successCallback();}
		else{ errorCallback();}
	});
}



// -----------------------------------

// get sequence number
express.get("/nto/get/admin/auth", (req, res) => {
	var seq = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for(var i=0; i < 200; i++){ seq += possible.charAt(Math.floor(Math.random() * possible.length));}

	var responseData = { "message": null, "data": null};
	connection.query("UPDATE `elections` SET `login_sequence`=? WHERE `election_id`=?", [seq, electionID], function (error, results, fields) {
		if(error){ responseData["message"] = "Unable to establish secure connection with db."; }

		responseData["message"] = "Sequence token generation successful.";
		responseData["data"] = seq;
		
		res.send(responseData);
	});
});

// validate sequence number
express.get("/nto/get/validated/sequence", (req, res) => {
	var responseData = { "message": null, "data": false };

	console.log("<trial> sequence: " + req.query.seq);

	var successCallback = function(){
		responseData["message"] = "Election sequence token confirmed.";
		responseData["data"] = true;
		res.send(responseData);
	}
	var errorCallback = function(){ 
		responseData["message"] = "Sequence token not found.";
		res.send(responseData);
	}	

	validateAdminToken(req.query.seq, res, null, responseData, successCallback, errorCallback);
});

// get voter to enable voting access
express.post("/nto/get/voter", (req, res) => {
	var responseData = { "message": null, "data": [] };

	var token = req.header("Authorization");
	var voter = req.body.voter;

	var successCallback = function(){
		connection.query("SELECT * FROM `voters` WHERE `election_id`=? and id_number=?", [electionID, voter], function (error, results, fields) {
			if(error){ responseData["message"] = "Unable to establish secure connection with db."; }
			
			if(results.length > 0){
				responseData["message"] = "Voter identified successfully.";
				responseData["data"] = results;
			}
			else{ responseData["message"] = "No voter found.";}

			console.log(responseData);

			res.send(responseData);
		});
	}
	var errorCallback = function(){
		responseData["message"] = "Searching for this voter failed.";
		res.send(responseData);
	}

	validateAdminToken(token, res, voter, responseData, successCallback, errorCallback);
});

// get apply voting access
express.post("/nto/update/voter", (req, res) => {
	var responseData = { "message": null, "data": [] };

	var token = req.header("Authorization");
	var voter = req.body;

	var successCallback = function(){
		connection.query("UPDATE `voters` SET `voting_enabled`=" + voter["voting_enabled"] + " WHERE `election_id`=? and `id_number`=?", [electionID, voter["id_number"]], function (error, results, fields) {
			if(error){ responseData["message"] = "Unable to establish secure connection with db."; }
			
			responseData["message"] = "Voter access changed.";
			responseData["data"] = 200;

			res.send(responseData);
		});
	}
	var errorCallback = function(){
		responseData["message"] = "Changing access failed.";
		res.send(responseData);
	}

	validateAdminToken(token, res, voter, responseData, successCallback, errorCallback);
});

// get logs
express.post("/nto/get/logs", (req, res) => {
	var responseData = { "message": null, "data": null };

	var token = req.header("Authorization");

	var successCallback = function(){
		fs.readFile(logName, "utf8", function (err, data) {
			if (err) { responseData["message"] = err;}
			
			responseData["message"] = "Log file contents.";
			responseData["data"] = data;

			res.send(responseData);
		});
	}
	var errorCallback = function(){
		responseData["message"] = "Unable to get logs.";
		res.send(responseData);
	}

	validateAdminToken(token, res, null, responseData, successCallback, errorCallback);
});

// get candidate list
express.post("/nto/get/candidates-list", (req, res) => {
	var responseData = { "message": null, "data": null };

	var token = req.header("Authorization");
	var senderData = req.body;

	var successCallback = function(){
		connection.query("SELECT * FROM `candidates` WHERE `election_id`=?", [electionID], function (error, results, fields) {
			if(error){ responseData["message"] = "Unable to establish secure connection with db."; }
			if(results.length > 0){
				responseData["message"] = "Candidates identified successfully.";
				responseData["data"] = results;
			}
			else{ responseData["message"] = "No candidates found.";}

			res.send(responseData);
		});
	}
	var errorCallback = function(){
		responseData["message"] = "Listing candidates failed.";
		res.send(responseData);
	}

	validateToken(token, res, senderData, responseData, successCallback, errorCallback, null);
});

// get position in order
express.post("/nto/get/positions-in-order", (req, res) => {
	var responseData = { "message": null, "data": null };

	var token = req.header("Authorization");
	var senderData = req.body;

	var successCallback = function(){
		connection.query("SELECT * FROM `elections` WHERE `election_id`=?", [electionID], function (error, results, fields) {
			if(error){ responseData["message"] = "Unable to establish secure connection with db."; }
			if(results.length > 0){
				responseData["message"] = "Election data retrieved.";
				responseData["data"] = results;
			}
			else{ responseData["message"] = "No election data found.";}
			res.send(responseData);
		});
	}
	var errorCallback = function(){
		responseData["message"] = "Election data doesn't exist.";
		res.send(responseData);
	}

	validateToken(token, res, senderData, responseData, successCallback, errorCallback, null);
});

// get voter login
express.get("/nto/get/voter-login", (req, res) => {
	var responseData = { "message": null, "data": null };
	connection.query("SELECT * FROM `voters` WHERE `id_number`=? and election_id=?", [req.query.id_number, electionID], function (error, results, fields) {
		if(error){ responseData["message"] = "Unable to establish secure connection with db."; }
		
		if(results.length == 1){
			// check if voting access has been enabled
			if(results[0]["voting_enabled"] == true){
				responseData["message"] = "Voter successfully retrieved.";
				responseData["data"] = results;
			}
			else{responseData["message"] = "Voter access not enabled.";}
		}
		else{responseData["message"] = "Voter not found.";}

		res.send(responseData);
	});
});

// add votes
express.post("/nto/add/votes", (req, res) => {
	var responseData = { "message": null, "data": null };

	var token = req.header("Authorization");
	var senderData = req.body;
	var voteCast = senderData["cast[]"];
	delete senderData["cast[]"];

	var successCallback = function(){
		var query = "INSERT INTO `ballot` (`election_id`, `voter_id_number`, `candidate_id_number`) VALUES ";
		for (var i = 0; i < voteCast.length; i++) {
			query = query + "("+ connection.escape(electionID) +", "+ connection.escape(senderData["id_number"]) +", "+ connection.escape(voteCast[i]) +")";
			if(i < voteCast.length-1){ query += ", ";}
		}

		connection.query(query, function (error, results, fields) {
			if(error){ responseData["message"] = "Unable to establish secure connection with db."; }

			console.log(query);
			// Note: the database accepts multiple votes per position, check the structure of the database for references.
			// this is a bad practice, the query may run without error but new data may not be inserted if indexes not proper.
			// execute a check query and modify the database to accept only specific n votes for specific position.
			responseData["message"] = "Voting successful. Thank you.";
			responseData["data"] = 200;

			connection.query("UPDATE `voters` SET `voting_enabled`=0, `vote_cast`=1, `on_vote`=CURRENT_TIMESTAMP WHERE `gen_id`=?", [senderData["gen_id"]]);
			res.send(responseData);
		});
	}
	var errorCallback = function(){
		responseData["message"] = "Reading voter's sheet failed. Please review voting format.";
		res.send(responseData);
	}

	validateToken(token, res, senderData, responseData, successCallback, errorCallback, voteCast);
});

// monitor voting
express.get("/nto/monitor/results", (req, res) => {
	var responseData = { "message": null, "data": null };
	connection.query("SELECT a.`party`, a.`position`, SUBSTRING_INDEX(a.`fullname`, \" \", 2) as 'fullname', a.`picture`, a.`id_number`, count(b.`candidate_id_number`) as 'votes' from `candidates` a left join `ballot` b on a.`id_number`=b.`candidate_id_number` WHERE a.`election_id`=? group by a.`id_number`", [electionID], function (error, results, fields) {
		if(error){ responseData["message"] = "Unable to establish secure connection with db."; }
		
		if(results.length != 0){
			responseData["message"] = "Voter successfully retrieved.";
			responseData["data"] = results;
		}
		else{responseData["message"] = "No results found.";}

		res.send(responseData);
	});
});

// -----------------------------------



// Proper error handling
express.use(function(req, res){ res.status(400).sendFile(reqPath.join(__dirname + "/../front-end-ballots/404.html"));});
express.use(function(error, req, res, next) { 
	console.log(error);
	res.status(500).sendFile(reqPath.join(__dirname + "/../front-end-ballots/500.html"));
});
// -----------------------------------



// NTO server and mysql instance
express.listen(config["default-port"], () => { 
	console.log("listening to port " + config["default-port"]);
	connection.connect(function(err){
		if(err){ console.error('error connecting: ' + err.stack);}
		console.log('connected as id ' + connection.threadId);
	});
});
// ---------------------------