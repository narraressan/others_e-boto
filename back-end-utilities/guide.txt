project format guides;
"reference-format-for-documentation":{
	"default-response-format": {
		"message": null,
		"data": null
	},
	"election-data-format": {
		"elections": {
			"election_id": electionID,
			"election_details": "School Year 2017-2018",
			"election_facilitators": "BA & A in partnership with <unsa na ba ang tawag sa BSIT na org karun?>",
			"positions": {
				"President": 1,
				"Vice-President": 1,
				"Secretary-General": 1,
				"Treasurer": 1,
				"Auditor": 1,
				"Councilors": 6
			},
			"login_sequence": ""
			"date_time": "current_timestamp"
		},
		"candidates": {
			"election_id": electionID,
			"party": null,
			"position": null,
			"fullname": null,
			"picture": null,
			"course": null,
			"id_number": null,
			"date_time": "current_timestamp"
		},
		"voters": {
			"election_id": electionID,
			"fullname": null,
			"course": null,
			"id_number": null,
			"voting_enabled": false,
			"vote_cast": false,
			"on_vote": "timestamp on vote",
			"date_time": "current_timestamp"
		},
		"ballot": {
			"election_id": electionID,
			"voter_id_number": null,
			"candidate_id_number": null,
			"date_time": "current_timestamp"
		}
	}
}
