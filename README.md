# NARRA-TINY-ORGANS
- e-Boto v2.0.0 -

This is a simple election system which caters your problems for secure voting, reliable and timely result counting
and etc. With this, you can implement an election (officers, board or directors, opinions, events, etc.) at ease.

Users:
	- College of BA & A -
		Institution:  Mindanao State University, General Santos City
		Facilitator: JITS
		Events: 
			S.Y. 2016-2017 SSC Elections
			S.Y. 2017-2018 - SSC Elections

Disclaimer
This is a very simple implementation of an election system. Usage of this simple app requires thorough study of the app.
Any modification or further improvements for the good and benefits is highly encouraged. Thank you.

- team NARRA -